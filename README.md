# ✨ IB DatePicker ✨
___~~~~~~____~~~~~~___
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![GitHub version](https://badge.fury.io/gh/Naereen%2FStrapDown.js.svg)](https://github.com/Naereen/StrapDown.js)
````````````
### Параметры

| Название | Тип | Значение по умолчанию | Описание |
| ------ | ------ | ------ | ------ |
| min | string / null | null | мин. дата доступная для выбора (iso) |
| max | string / null | null | макс. дата доступная для выбора (iso) |
| without-shadow | boolean | false | скрыть рамку/тень |
| multiple | boolean | false | возможность множественного выбора |
| range | boolean | false | Вкл./откл. range (multiple отключается) |
| v-model | string[] | [] | Выбранные даты (iso). Если !range && !multiple = [ string ], иначе, если range = [ string, string ], иначе, если multiple = string[] |
| disable-dates | string[] | [] | Даты недоступные для выбора (iso) |
| start-range | boolean | false | Устанавливать как начальный range |
| finish-range | boolean | false | Устанавливать как последний range |
| day-locale | string[7] | [RU] | Локализация дней недели |
| month-locale | string[12] | [RU] | Лоаклизация месяцев |
| full-width | boolean | false | Растягивать на всю ширину |

### Пример по умоланию

#### ✨ Vue ✨

> <IBDatePicker\
> &nbsp;&nbsp;&nbsp;&nbsp;v-model="[]"\
> &nbsp;&nbsp;&nbsp;&nbsp;:min="null"\
> &nbsp;&nbsp;&nbsp;&nbsp;:max="null"\
> &nbsp;&nbsp;&nbsp;&nbsp;:without-shadow="false"\
> &nbsp;&nbsp;&nbsp;&nbsp;:multiple="false"\
> &nbsp;&nbsp;&nbsp;&nbsp;:range="false"\
> &nbsp;&nbsp;&nbsp;&nbsp;:day-locale="[...RU]"\
> &nbsp;&nbsp;&nbsp;&nbsp;:month-locale="[...RU]"\
> />

## License

MIT

**Free Software**

---

Flaws:

- set range from model
- range for desktop responsive
- min-with responsive
- set window from param (DAYS, MONTHS, YEARS)
- start/last ranges animation if full width