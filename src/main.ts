import Vue from 'vue';
import './style/main.sass';
import './style/ONLY-DEV.sass';
import App from "@/App.vue";

Vue.config.productionTip = false;

new Vue({
	render: (h) => h(App)
}).$mount('#app');
