import { Prop, Vue, Component } from 'vue-property-decorator';

@Component
export default class UDateYearProps extends Vue {

	@Prop({ type: Number, default: 15 })
	readonly yearsCount!: number;

}