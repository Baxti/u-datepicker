import { Component, Vue, Prop } from 'vue-property-decorator';
import { U_DATEPICKER_MAX_DATE, U_DATEPICKER_MIN_DATE } from '@/core/config';
import { TDatePickerLimit, TDatePickerModel } from '@/types/interface';

@Component
export default class UDatePickerLimitProps extends Vue {

	@Prop({ type: String, default: U_DATEPICKER_MIN_DATE })
	readonly min!: TDatePickerLimit;

	@Prop({ type: String, default: U_DATEPICKER_MAX_DATE })
	readonly max!: TDatePickerLimit;

	@Prop({ type: Array, default: undefined })
	readonly disabledDates: TDatePickerModel;

}