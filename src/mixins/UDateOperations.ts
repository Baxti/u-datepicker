import { Component, Vue } from 'vue-property-decorator';
import moment from 'moment';
import { IDatePickerDateNum, IDatePickerDay } from '@/types/interface';

@Component
export default class UDateOperations extends Vue {

	protected yearAndMonthFormat: string = 'YYYY-MM';

	protected getCurrentYear(): number {
		return (new Date()).getFullYear();
	}

	protected getCurrentMonth(): number {
		return (new Date()).getMonth() + 1;
	}

	protected getPrevMonth(month: number): number {
		return month <= 1 ? 12 : --month;
	}

	protected getNextMonth(month: number): number {
		return month >= 12 ? 1 : ++month;
	}

	protected getNextISOYearAndMonth(month: number, year: number): string {
		const nextMonth: number = this.getNextMonth(month);
		const nextISOMonth: string = this.getDateWithZeroPrefix(nextMonth);

		return (nextMonth <= 1 ? `${ ++year }-` : `${ year }-`) + nextISOMonth;
	}

	protected getPrevISOYearAndMonth(month: number, year: number): string {
		const prevMonth: number = this.getPrevMonth(month);
		const prevISOMonth: string = this.getDateWithZeroPrefix(prevMonth);

		return (prevMonth >= 12 ? `${ --year }-` : `${ year }-`) + prevISOMonth;
	}

	protected getDateWithZeroPrefix(dayOrMonth: number): string {
		return dayOrMonth < 10 ? '0' + dayOrMonth : String(dayOrMonth);
	}

	protected getISOYearAndMonth(month: number, year: number): string {
		return `${ year }-${ this.getDateWithZeroPrefix(month) }`;
	}

	protected getISODate(month: number, year: number, day: number): string {
		return this.getISOYearAndMonth(month, year) + '-' + this.getDateWithZeroPrefix(day);
	}

	protected getISODateByDay({ month, year, day }: IDatePickerDay): string {
		return this.getISODate(month, year, day);
	}

	protected getDaysInMonthByISO(isoYearAndMonth: string): number {
		return moment(isoYearAndMonth, this.yearAndMonthFormat).daysInMonth();
	}

	protected getDaysInMonth(month: number, year: number): number {
		return this.getDaysInMonthByISO(this.getISOYearAndMonth(month, year));
	}

	protected getWeekDay(day: number, month: number, year: number): number {
		const week: number = moment(this.getISOYearAndMonth(month, year) +
			'-' +
			this.getDateWithZeroPrefix(day)).weekday();

		return week === 0 ? 7 : week;
	}

	protected getDateNumByISOStr(v: string): number {
		return +(v.split('-').join('')) || 0;
	}

	protected getDateNumsByISOStr(v: string): IDatePickerDateNum | null {
		const dateArr: string[] = v.split('-');

		if (!dateArr[0] || !dateArr[1]) return null;

		return {
			year: +dateArr[0],
			day: +dateArr[2] || 0,
			month: +dateArr[1]
		};
	}

}