import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class UDateMonthProps extends Vue {

	@Prop({
		type: Array,
		default: () => [
			'Январь',
			'Февраль',
			'Март',
			'Апрель',
			'Май',
			'Июнь',
			'Июль',
			'Август',
			'Сентябрь',
			'Октябрь',
			'Ноябрь',
			'Декабрь'
		]
	})
	readonly monthLocale!: string[];

}