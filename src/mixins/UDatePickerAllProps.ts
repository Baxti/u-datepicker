import { Component, Prop, Mixins } from 'vue-property-decorator';
import UDatePickerMainProps from '@/mixins/UDatePickerMainProps';

@Component
export default class UDatePickerAllProps extends Mixins(UDatePickerMainProps) {

	@Prop({ type: Boolean, default: false })
	readonly multiple!: boolean;

	@Prop({ type: Boolean, default: false })
	readonly range!: boolean;

	@Prop({ type: Boolean, default: false })
	readonly startRange!: boolean;

	@Prop({ type: Boolean, default: false })
	readonly finishRange!: boolean;

	@Prop({ type: Boolean, default: false })
	readonly fullWidth!: boolean;

}