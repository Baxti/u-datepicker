import { Component, Prop, Vue } from 'vue-property-decorator';
import { TDatePickerModel } from '@/types/interface';

@Component
export default class UDatePickerMainProps extends Vue {

	@Prop({ type: Array, default: undefined })
	readonly value!: TDatePickerModel;

	@Prop({
		type: Array,
		default: () => [
			'ПН',
			'ВТ',
			'СР',
			'ЧТ',
			'ПТ',
			'СБ',
			'ВС'
		]
	})
	readonly dayLocale!: string[];

	@Prop({ type: Boolean, default: false })
	readonly withoutShadow!: boolean;

}