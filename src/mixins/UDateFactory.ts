import { Component, Mixins } from 'vue-property-decorator';
import { IDatePickerDateConfig, IDatePickerDay } from '@/types/interface';
import UDateLimits from '@/mixins/UDateLimits';

@Component
export default class UDateFactory extends Mixins(UDateLimits) {

	protected getDefaultDate(isActive: boolean = false): IDatePickerDateConfig {
		return {
			isActive,
			isLastRange: false,
			isStartRange: false
		};
	}

	protected getDatesByMonth(month: number, year: number): IDatePickerDay[] {
		const days: IDatePickerDay[] = [];

		let daysCount: number = this.getDaysInMonth(month, year);

		while (daysCount--) {
			const day: number = daysCount + 1;

			days.unshift({
				...this.getDefaultDate(),
				month,
				day,
				year,
				isOut: false,
				disabled: this.isDisabledDay(month, year, day)
			});
		}

		return days;
	}

	protected getDatesOnLeftByMonth(month: number, year: number): IDatePickerDay[] {
		let weekDay: number = this.getWeekDay(1, month, year);

		const prevDays: IDatePickerDay[] = [];

		if (weekDay <= 1) return prevDays;

		const prevISOYearAndMonth: string = this.getPrevISOYearAndMonth(month, year);
		const prevMonth: number = this.getPrevMonth(month);
		const prevYear: number = +(prevISOYearAndMonth.split('-')[0]);

		let prevMonthDays: number = this.getDaysInMonthByISO(prevISOYearAndMonth);

		while (--weekDay) {
			const day: number = prevMonthDays--;

			prevDays.unshift({
				...this.getDefaultDate(),
				month: prevMonth,
				day,
				year: prevYear,
				isOut: true,
				disabled: this.isDisabledDay(prevMonth, prevYear, day)
			});
		}

		return prevDays;
	}

	protected getDatesOnRightByMonth(month: number, year: number): IDatePickerDay[] {
		let weekDay: number = this.getWeekDay(this.getDaysInMonth(month, year), month, year);

		const nextDays: IDatePickerDay[] = [];

		if (weekDay >= 7) return nextDays;

		const nextISOYearAndMonth: string = this.getNextISOYearAndMonth(month, year);
		const nextMonth: number = this.getNextMonth(month);
		const nextYear: number = +(nextISOYearAndMonth.split('-')[0]);

		let nextMonthDays: number = 1;

		weekDay = 7 - weekDay;

		while (weekDay--) {
			const day: number = nextMonthDays++;

			nextDays.push({
				...this.getDefaultDate(),
				month: nextMonth,
				day,
				year: nextYear,
				isOut: true,
				disabled: this.isDisabledDay(nextMonth, nextYear, day)
			});
		}

		return nextDays;
	}

	protected getFilledMonthDates(month: number, year: number): IDatePickerDay[] {
		return [
			...this.getDatesOnLeftByMonth(month, year),
			...this.getDatesByMonth(month, year),
			...this.getDatesOnRightByMonth(month, year)
		];
	}

}