import { Mixins, Component } from 'vue-property-decorator';
import UDateYearOperations from '@/mixins/UDateYearOperations';
import { IDatePickerDateNum, IDatePickerMonth } from '@/types/interface';
import UDateMonthProps from '@/mixins/UDateMonthProps';

@Component
export default class UDateMonthOperations extends Mixins(UDateYearOperations, UDateMonthProps) {

	public currentMonth: number = 0;

	public months: IDatePickerMonth[] = [];

	protected initMonths(): this {
		this.currentMonth = this.getCurrentMonth();

		return this.setMonthsList();
	}

	protected setPrevMonth() {
		this.currentMonth = (this.currentMonth === this.minMonth && this.currentYear === this.minYear)
			? this.minMonth
			: this.getPrevMonth(this.currentMonth);

		if (this.currentMonth >= 12) this.setPrevYear();
	}

	protected setNextMonth() {
		this.currentMonth = (this.currentMonth === this.maxMonth && this.currentYear === this.maxYear)
			? this.maxMonth
			: this.getNextMonth(this.currentMonth);

		if (this.currentMonth <= 1) this.setNextYear();
	}

	protected setMonthsList(currentMonth: number = this.currentMonth, currentYear: number = this.currentYear): this {
		this.months = this.monthLocale.map((i, idx) => {
			const month: number = idx + 1;

			return {
				...this.getDefaultDate(month === this.currentMonth),
				month,
				title: i,
				year: currentYear,
				disabled: this.isDisabledMonth(month, currentYear)
			};
		});

		return this;
	}

	protected updateMonthListFromCurrentYear(): this {
		this.months.forEach(i => {
			i.disabled = this.isDisabledMonth(i.month, this.currentYear);
			i.year = this.currentYear;
			i.isActive = (this.currentMonth === i.month);
		});

		return this;
	}

	protected setCurrentYearAndMonthByFirstSelectedDay(selectedDay: string) {
		const dateNum: IDatePickerDateNum | null = this.getDateNumsByISOStr(selectedDay);

		if (!dateNum) return;

		this.currentYear = dateNum.year;
		this.currentMonth = dateNum.month;
	}

	public setMonth(month: IDatePickerMonth) {
		this.clearActiveMonth();

		month.isActive = true;

		this.currentMonth = month.month;

		this.setStep('MONTH');
	}

	protected clearActiveMonth(): this {
		for (let i = 0; i < this.months.length; i++) {
			if (this.months[i].isActive) {
				this.months[i].isActive = false;

				break;
			}
		}

		return this;
	}

	protected clearMonthRanges(): this {
		this.months.forEach(i => i.isStartRange = i.isLastRange = false);

		return this;
	}

	get currentMonthTitle(): string {
		try {
			return this.months.find(i => i.month === this.currentMonth)!.title || '';
		} catch (err) {
			console.warn('get currentMonthTitle()', err);

			return '';
		}
	}

}
