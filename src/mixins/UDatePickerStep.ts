import { Component, Vue } from 'vue-property-decorator';
import { TDatePickerStep } from '@/types/interface';

@Component
export default class UDatePickerStep extends Vue {

	public step: TDatePickerStep = 'DAY';

	public setStep(newStep: TDatePickerStep) {
		requestAnimationFrame(() => this.step = (this.step === newStep)
			? (newStep === 'YEAR' ? 'MONTH' : 'DAY')
			: newStep);
	}

}