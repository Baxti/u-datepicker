import { Component, Mixins } from 'vue-property-decorator';
import { IDatePickerYear } from '@/types/interface';
import UDateFactory from '@/mixins/UDateFactory';
import UDateYearProps from '@/mixins/UDateYearProps';
import UDatePickerStep from '@/mixins/UDatePickerStep';

@Component
export default class UDateYearOperations extends Mixins(
	UDateFactory,
	UDateYearProps,
	UDatePickerStep
) {

	protected lastYear: number = 0;

	public currentYear: number = 0;

	public years: IDatePickerYear[] = [];

	protected initYears(): this {
		this.lastYear = this.currentYear = this.getCurrentYear();

		return this.setYearsList();
	}

	protected setYearsListByLastYear(lastYear: number) {
		if (this.lastYear !== lastYear) {
			this.lastYear = lastYear;

			this.setYearsList(this.lastYear);
		}
	}

	protected setNextYearsList() {
		let lastYear: number = this.lastYear + this.yearsCount;

		if (lastYear > this.maxYear) {
			if ((lastYear - this.yearsCount) <= this.maxYear) {
				lastYear = this.lastYear;
			}
		}

		this.setYearsListByLastYear(lastYear);
	}

	protected setPrevYearsList() {
		let lastYear: number = this.lastYear - this.yearsCount;

		if (lastYear < this.minYear) {
			if ((lastYear + this.yearsCount) <= this.minYear) {
				lastYear = this.lastYear;
			}
		}

		this.setYearsListByLastYear(lastYear);
	}

	protected setPrevYear() {
		const currentYear: number = this.currentYear - 1;

		this.currentYear = this.minYear > currentYear
			? this.minYear
			: currentYear;
	}

	protected setNextYear() {
		const currentYear: number = this.currentYear + 1;

		this.currentYear = this.maxYear < currentYear
			? this.maxYear
			: currentYear;
	}

	protected setYearsList(currentYear: number = this.currentYear) {
		let { yearsCount } = this;

		this.years = [];

		while (yearsCount--) {
			const year: number = yearsCount + currentYear;

			this.years.unshift({
				...this.getDefaultDate(this.currentYear === year),
				year,
				disabled: this.isDisabledYear(year)
			});
		}

		return this;
	}

	protected updateYearsListFromCurrentYear(): this {
		this.years.forEach(i => {
			i.disabled = this.isDisabledYear(i.year);
			i.isActive = (this.currentYear === i.year);
		});

		return this;
	}

	public setCurrentYearIsActive(): this {
		for (let i = 0; i < this.years.length; i++) {
			if (this.years[i].year === this.currentYear) {
				this.years[i].isActive = true;

				break;
			}
		}

		return this;
	}

	public currentYearExistInList(): boolean {
		return (this.lastYear <= this.currentYear
			&& this.currentYear <= (this.lastYear + this.yearsCount));
	}

	public showCurrentYear(): this {
		if (this.currentYearExistInList()) return this;

		this[this.currentYear > this.lastYear ? 'setNextYearsList' : 'setPrevYearsList']();

		return this.showCurrentYear();
	}

	public setYear(year: IDatePickerYear) {
		this.clearActiveYear();

		year.isActive = true;

		this.currentYear = year.year;

		this.setStep('YEAR');
	}

	protected clearActiveYear(): this {
		for (let i = 0; i < this.years.length; i++) {
			if (this.years[i].isActive) {
				this.years[i].isActive = false;

				break;
			}
		}

		return this;
	}

	protected clearYearRanges(): this {
		this.years.forEach(i => i.isStartRange = i.isLastRange = false);

		return this;
	}

}