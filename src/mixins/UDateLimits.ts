import { Component, Mixins } from 'vue-property-decorator';
import { IDatePickerDateNum, TDatePickerLimit, TDatePickerSelectedDay } from '@/types/interface';
import UDateOperations from '@/mixins/UDateOperations';
import UDatePickerLimitProps from '@/mixins/UDatePickerLimitProps';
import { U_DATEPICKER_MAX_DATE, U_DATEPICKER_MIN_DATE } from '@/core/config';

@Component
export default class UDateLimits extends Mixins(UDateOperations, UDatePickerLimitProps) {

	protected regexForValidateISODate: RegExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;

	protected maxYear: number = 0;

	protected minYear: number = 0;

	protected maxMonth: number = 0;

	protected minMonth: number = 0;

	protected maxDay: number = 0;

	protected minDay: number = 0;

	public getLocalISOMax(): string {
		return this.getISODate(this.maxMonth, this.maxYear, this.maxDay);
	}

	public getLocalISOMin(): string {
		return this.getISODate(this.minMonth, this.minYear, this.minDay);
	}

	protected inDisabledDatesList(v: string): boolean {
		return (this.disabledDates || []).includes(v);
	}

	protected isDisabledDay(month: number, year: number, day: number): boolean {
		const currentISO: string = this.getISODate(month, year, day);
		const current: number = this.getDateNumByISOStr(currentISO);
		const min: number = this.getDateNumByISOStr(this.getLocalISOMin());
		const max: number = this.getDateNumByISOStr(this.getLocalISOMax());

		return ((current > max)
			|| (current < min)
			|| this.inDisabledDatesList(currentISO));
	}

	protected isDisabledISODate(v: TDatePickerSelectedDay): boolean {
		if (!v) return true;

		const dateNum: IDatePickerDateNum | null = this.getDateNumsByISOStr(v);

		if (!dateNum) return true;

		return this.isDisabledDay(dateNum.month, dateNum.year, dateNum.day);
	}

	protected isDisabledYear(year: number): boolean {
		return (this.maxYear < year || this.minYear > year);
	}

	protected isDisabledMonth(month: number, currentYear: number): boolean {
		const current: number = this.getDateNumByISOStr(this.getISOYearAndMonth(month, currentYear));
		const min: number = this.getDateNumByISOStr(this.getISOYearAndMonth(this.minMonth, this.minYear));
		const max: number = this.getDateNumByISOStr(this.getISOYearAndMonth(this.maxMonth, this.maxYear));

		return (current < min || current > max);
	}

	protected isValidISODate(v: TDatePickerSelectedDay) {
		return this.regexForValidateISODate.test(v || '');
	}

	protected setMinByISOStr(v: TDatePickerLimit = this.min): this {
		const dateNums: IDatePickerDateNum = this.getDateNumsByISOStr(v || U_DATEPICKER_MIN_DATE) as IDatePickerDateNum;

		this.minYear = dateNums.year;
		this.minMonth = dateNums.month;
		this.minDay = dateNums.day;

		return this;
	}

	protected setMaxByISOStr(v: TDatePickerLimit = this.max): this {
		const dateNums: IDatePickerDateNum = this.getDateNumsByISOStr(v || U_DATEPICKER_MAX_DATE) as IDatePickerDateNum;

		this.maxYear = dateNums.year;
		this.maxMonth = dateNums.month;
		this.maxDay = dateNums.day;

		return this;
	}

}