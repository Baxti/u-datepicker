export enum EDatePickerType {
	ONLY_DAY = 'ONLY_DAY',
	MULTIPLE_DAYS = 'MULTIPLE_DAYS',
	DAYS_RANGE = 'DAYS_RANGE'
}

export interface IDatePickerDateConfig {
	isActive: boolean
	isStartRange: boolean
	isLastRange: boolean
}

export interface IDatePickerYear extends IDatePickerDateConfig {
	year: number
	disabled: boolean
}

export interface IDatePickerMonth extends IDatePickerYear {
	month: number
	title?: string
}

export interface IDatePickerDay extends IDatePickerMonth {
	day: number
	isOut?: boolean
}

export interface IDatePickerDateNum {
	day: number
	month: number
	year: number
}

export interface IDatePickerDate {
	day?: IDatePickerDay
	month?: IDatePickerMonth
	year?: IDatePickerYear
}

export type TDatePickerDayKey = keyof IDatePickerDay

export type TDatePickerMonthKey = keyof IDatePickerMonth

export type TDatePickerYearKey = keyof IDatePickerYear

export type TDatePickerDayVal = IDatePickerDay[TDatePickerDayKey]

export type TDatePickerMonthVal = IDatePickerMonth[TDatePickerMonthKey]

export type TDatePickerYearVal = IDatePickerYear[TDatePickerYearKey]

export interface IDatePickerSelectedDate extends IDatePickerDate {
	setDayProp: (key: TDatePickerDayKey, val: TDatePickerDayVal) => IDatePickerSelectedDate
	setMonthProp: (key: TDatePickerMonthKey, val: TDatePickerMonthVal) => IDatePickerSelectedDate
	setYearProp: (key: TDatePickerYearKey, val: TDatePickerYearVal) => IDatePickerSelectedDate
	setMainProps: (key: TDatePickerYearKey, val: TDatePickerYearVal) => IDatePickerSelectedDate
}

export type TDatePickerStep = 'DAY'
	| 'MONTH'
	| 'YEAR';

export type TDatePickerType = keyof typeof EDatePickerType;

export type TDatePickerSelectedDay = string | null | undefined;

export type TDatePickerSelectedDays = Array<TDatePickerSelectedDay>;

export type TDatePickerModel = string[] | undefined | null;

export type TDatePickerLimit = string | null;